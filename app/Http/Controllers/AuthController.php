<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('auth.register');
    }
    public function welcome(Request $request){
        $namaDepan = $request->depan;
        $namaBelakang = $request->belakang  ;

        //dd($nameDepan);
        //return view('auth.welcome');
        return view('auth.login', compact('namaDepan','namaBelakang'));
    }

}
